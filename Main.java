/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.io.*;
import java.util.*;
public class Main
{ 
    public static void main(String[] args) 
  {  
      Scanner in = new Scanner(System.in);
      String str = in.nextLine();
      String[] subStr;
      subStr = str.split(" ");
      int uzel = Integer.parseInt(subStr[0]);
      int otrezok = Integer.parseInt(subStr[1]);
      for(int i = 1; i <= Math.sqrt(uzel); i++) {
          if(i*(uzel/i)==uzel){
              if(i*(uzel/i+1)+(uzel/i)*(i+1)==otrezok){
                  int a = i+1;
                  int b = uzel/i+1;
                  System.out.println(String.format("%s %s", a,b));
              }

          }
       }

  }
}
